import {Component} from "@angular/core";
import * as firebase from "firebase/app";
import "firebase/auth";
import {NavController} from "@ionic/angular";
import OAuthCredential = firebase.auth.OAuthCredential;

@Component({
    selector: "app-home",
    templateUrl: "home.page.html",
    styleUrls: ["home.page.scss"],
})
export class HomePage {

    public userProfile: any = null;

    constructor(public navCtrl: NavController) {
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                console.log(user);
                this.userProfile = user;
            } else {
                console.log("There's no user here");
            }
        });
    }

    googleLogin(): void {
        const provider = new firebase.auth.GoogleAuthProvider();
        console.log("I was clicked");

        firebase.auth().signInWithRedirect(provider).then(() => {
            console.log("Stage 2!");
            firebase.auth().getRedirectResult().then(result => {
                // This gives you a Google Access Token.
                // You can use it to access the Google API.
                const token = ((result.credential) as OAuthCredential).accessToken;
                // The signed-in user info.
                const user = result.user;
                console.log(token, user);
            }).catch((error) => {
                // Handle Errors here.
                console.error(error.message);
            });
        });
    }
}
