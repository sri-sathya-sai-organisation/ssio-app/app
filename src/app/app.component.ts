import {Component} from "@angular/core";

import {Platform} from "@ionic/angular";
import {SplashScreen} from "@ionic-native/splash-screen/ngx";
import {StatusBar} from "@ionic-native/status-bar/ngx";
import * as firebase from "firebase/app";
import "firebase/auth";
import OAuthCredential = firebase.auth.OAuthCredential;

@Component({
    selector: "app-root",
    templateUrl: "app.component.html",
    styleUrls: ["app.component.scss"]
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar
    ) {
        this.initializeApp();
        firebase.initializeApp({
            apiKey: "AIzaSyDh9JdD5fQIFjkysCPmGoWx2TwV03xOqnY",
            authDomain: "sathya-sai-ya-app.firebaseapp.com",
            databaseURL: "https://sathya-sai-ya-app.firebaseio.com",
            projectId: "sathya-sai-ya-app",
            storageBucket: "sathya-sai-ya-app.appspot.com",
            messagingSenderId: "114044895118",
        });
        firebase
            .auth()
            .getRedirectResult()
            .then((result) => {
                if (result.credential) {
                    const token = ((result.credential) as OAuthCredential).accessToken;
                    const user = result.user;
                    console.log(token, user);
                }
            })
            .catch((error) => {
                // Handle Errors here.
                const errorMessage = error.message;
                console.log(errorMessage);
                console.log (error.stack);
            });
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
}
