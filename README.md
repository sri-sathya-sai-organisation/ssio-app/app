#### Building your android client
TBD

#### Building your IoS client
TBD

#### Run on docker with docker-compose for NG App Only
0. Pre-req: Install [docker](https://docs.docker.com/desktop/) and docker-compose: ```brew install docker-compose```
1. Make sure you are in ```scripts/docker/app``` directory, you use `cd ../scripts/docker/app` from the current directory
1. Run ```docker-compose up```
1. Once you see in your terminal 
```
app_server | 2020-05-05T13:46:45.040Z [info] =============API Server Instance started on http://c84cc971f133:3006=============
```
Open your browser and navigate to http://localhost:3006/documentation

